/*
 * Return a string str with string end removed from its end.
 * str: String
 * end: String
 */
export function strip(str, end) {
  return str.endsWith(end) ? str.slice(0, -1 * end.length) : str;
}
