## sumika.js

[![pipeline status](https://gitlab.com/aos2/sumikat/badges/master/pipeline.svg)](https://gitlab.com/aos2/sumikat/commits/master)

[GraphQL][graphql-js] server for the [Acceleration of Suguri 2 community][aos2] using [pg-promise] to interface with a [Postgres][postgres] database. As [express-graphql does not support ECMAScript Modules despite graphql-js and graphql-relay doing so][express-graphql-433], we opt for [graphql-api-koa], a middleware that does. A `Dockerfile` is supplied for running in a [minimal Alpine Linux environment][images] allowing for use on machines without natively installing [node].

## requirements

* [docker]
* [node] for development

```sh
git clone git@gitlab.com:aos2/sumikat.git
cd sumikat
docker build -t sumikat .
# Use host network (trivially connect to DB on host)
# Publish port 4000 onto host port 4000
docker run --network=host -p 4000:4000 sumikat
```

## development

Install development tools with `npm install --only=dev` which includes a pre-commit hook that checks for errors reported by [prettier] and [eslint]. Use `npm run lint` to automatically adjust files in-place.


[aos2]: https://aos2.gitlab.io
[docker]: https://www.docker.com/
[eslint]: https://eslint.org/
[express-graphql-433]: https://github.com/graphql/express-graphql/pull/433#issuecomment-391223747
[graphql-js]: https://github.com/graphql/graphql-js/
[graphql-api-koa]: https://github.com/jaydenseric/graphql-api-koa/
[images]: https://hub.docker.com/r/mhart/alpine-node/
[node]: https://nodejs.org/en/
[pg-promise]: https://github.com/vitaly-t/pg-promise/
[prettier]: http://prettier.io/
[postgres]: https://www.postgresql.org/
