import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString
} from "graphql";
import ctags from "common-tags";
import pgp from "pg-promise";
import secret from "./secret.json";
import { strip } from "./utils.mjs";
import * as matchup from "./resolve/match.mjs";
import * as console from "./log.mjs";

const { oneLine } = ctags;
const postgres = pgp();
const db = postgres(secret.psql);
/* Assign relatively large constants to order non-numbered rounds. */
const roundOrder = oneLine`
  ORDER BY
    match_date $[order:raw],
    (CASE WHEN round = 'Grand Finals' THEN 999
          WHEN round = 'Loser''s Finals' THEN 998
          WHEN round = 'Winner''s Finals' THEN 997
          WHEN round = 'Finals' THEN 996
          WHEN round = 'Bronze' THEN 995
          WHEN round = 'Semifinals' THEN 994
          WHEN round LIKE 'Ro%' THEN 993 - substring(round from 3)::integer
          WHEN round = 'Exhibition' THEN 0
          ELSE round::integer
     END) $[order:raw],
     id $[order:raw]
`;

function format(query) {
  return values => {
    const q = pgp.as.format(query, values);
    console.log(q);
    return q;
  };
}

const event = new GraphQLObjectType({
  name: "event",
  fields: () => ({
    id: { type: GraphQLID },
    event: { type: GraphQLString },
    event_date: { type: GraphQLString },
    completed: { type: GraphQLBoolean },
    link_signup: { type: GraphQLString },
    link_bracket: { type: GraphQLString },
    description: { type: GraphQLString },
    matches: {
      type: new GraphQLList(match),
      resolve(parent, _) {
        let query = 'SELECT * FROM "matches" WHERE event=$[event]';
        query += " ";
        query += roundOrder;
        return db
          .any(query, { event: parent.event, order: "asc" })
          .catch(console.error);
      }
    },
    meta: metaInfo
  })
});

const eventByID = {
  type: event,
  args: {
    id: { type: GraphQLNonNull(GraphQLID) }
  },
  resolve(_, args) {
    const query = format('SELECT * FROM "schedule" WHERE id=$1;');
    return db.one(query, args.id).catch(console.error);
  }
};

const eventsUpcoming = {
  type: new GraphQLList(event),
  resolve() {
    const query = format('SELECT * FROM "schedule" WHERE completed=False;');
    return db.any(query).catch(console.error);
  }
};

const eventsCompleted = {
  type: new GraphQLList(event),
  args: {
    limit: { type: GraphQLInt },
    offset: { type: GraphQLInt }
  },
  resolve(_, args) {
    args.limit = args.limit ? args.limit : 10;
    args.offset = args.offset ? args.offset : 0;
    const query = format(oneLine`
      SELECT * FROM "schedule" WHERE completed=True
      ORDER BY event_date desc LIMIT $[limit] OFFSET $[offset];`);
    return db.any(query, args).catch(console.error);
  }
};

const match = new GraphQLObjectType({
  name: "match",
  fields: {
    id: { type: GraphQLID },
    match_date: { type: GraphQLString },
    timestamp: { type: GraphQLString },
    event: { type: GraphQLString },
    round: { type: GraphQLString },
    version: { type: GraphQLString },
    p1_name: { type: GraphQLString },
    p1_char: { type: GraphQLString },
    p2_name: { type: GraphQLString },
    p2_char: { type: GraphQLString },
    p2_win: { type: GraphQLBoolean }
  }
});

const matchByID = {
  type: match,
  args: {
    id: { type: GraphQLNonNull(GraphQLID) }
  },
  resolve(_, args) {
    const query = format('SELECT * FROM "matches" WHERE id=$1;');
    return db.one(query, args.id).catch(console.error);
  }
};

const matches = {
  type: new GraphQLList(match),
  args: {
    char1: { type: GraphQLString },
    char2: { type: GraphQLString },
    name1: { type: GraphQLString },
    name2: { type: GraphQLString },
    slot1_win: { type: GraphQLBoolean },
    after_date: { type: GraphQLString },
    before_date: { type: GraphQLString },
    round: { type: GraphQLString },
    version: { type: GraphQLString },
    order: { type: GraphQLString },
    limit: { type: GraphQLString },
    offset: { type: GraphQLInt }
  },
  resolve(_, args) {
    args.order = args.order ? args.order : "desc";
    args.limit = args.limit ? args.limit : 10;
    args.offset = args.offset ? args.offset : 0;
    let query = 'SELECT * FROM "matches" WHERE ';
    if (Object.keys(args).length) {
      query += matchup.filter({ ...args });
    }
    query = strip(query, " WHERE ");
    query += " ";
    query += roundOrder;
    query += " LIMIT $[limit:raw] OFFSET $[offset]";
    query = format(query.trim() + ";");
    return db.any(query, args).catch(console.error);
  }
};

const meta = new GraphQLObjectType({
  name: "meta",
  fields: {
    dates: {
      type: new GraphQLList(GraphQLString),
      resolve(parent, _) {
        const byEvent = parent && parent.event ? "WHERE event=$[event]" : "";
        const query = format(oneLine`
          SELECT max(match_date) AS dates FROM "matches" ${byEvent}
          UNION SELECT min(match_date) FROM "matches" ${byEvent}
          ORDER BY dates asc;
        `);
        return db
          .any(query, parent)
          .then(src => src.map(e => e.dates))
          .catch(console.error);
      }
    },
    names: {
      type: new GraphQLList(GraphQLString),
      resolve(parent, _) {
        const byEvent = parent && parent.event ? "WHERE event=$[event]" : "";
        const query = format(oneLine`
          SELECT DISTINCT p1_name AS names FROM "matches" ${byEvent}
          UNION SELECT p2_name FROM "matches" ${byEvent}
          ORDER BY names asc;
        `);
        return db
          .any(query, parent)
          .then(src => src.map(e => e.names))
          .catch(console.error);
      }
    },
    versions: {
      type: new GraphQLList(GraphQLString),
      resolve(parent, _) {
        const byEvent = parent && parent.event ? "WHERE event=$[event]" : "";
        const query = format(oneLine`
          SELECT DISTINCT version AS versions FROM "matches" ${byEvent}
          ORDER BY versions asc;
        `);
        return db
          .any(query, parent)
          .then(src => src.map(e => e.versions))
          .catch(console.error);
      }
    }
  }
});
const metaInfo = {
  type: meta,
  resolve: parent => {
    /* Pass an object for field resolvers to work with */
    return parent || {};
  }
};

const root = new GraphQLObjectType({
  name: "root",
  fields: {
    match: matchByID,
    matches: matches,
    meta: metaInfo,
    event: eventByID,
    completed: eventsCompleted,
    upcoming: eventsUpcoming
  }
});

export default new GraphQLSchema({
  query: root
});
