import ctags from "common-tags";
import { strip } from "../utils.mjs";

const { oneLine } = ctags;

export function filter(args) {
  let query = "";
  if (args.hasOwnProperty("after_date")) {
    query += "match_date>=$[after_date]";
    query += " AND ";
  }
  if (args.hasOwnProperty("before_date")) {
    query += "match_date<=$[before_date]";
    query += " AND ";
  }
  if (args.hasOwnProperty("char1") || args.hasOwnProperty("name1")) {
    query += matchup(1, args.char1, args.name1, args.slot1_win);
    query += " AND ";
  }
  if (args.hasOwnProperty("char2") || args.hasOwnProperty("name2")) {
    query += matchup(2, args.char2, args.name2, args.slot1_win);
    query += " AND ";
  }
  if (args.hasOwnProperty("round")) {
    query += "round=$[round]";
    query += " AND ";
  }
  if (args.hasOwnProperty("version")) {
    query += "version=$[version]";
  }
  return strip(query, " AND ");
}

/*
 * Returns a string representing a SQL condition requiring a matchup
 * to meet certain criteria for one side of a matchup, choosing the
 * correct branch by checking which constraints are defined.
 * side: 1 | 2
 * char?: any
 * name?: any
 * win?: any
 */
function matchup(side, char, name, win) {
  const hasChar = char !== undefined;
  const hasName = name !== undefined;
  const hasWin = win !== undefined;
  char = "char" + side;
  name = "name" + side;
  const win1 =
    side === 1 ? "p2_win!=$[slot1_win:raw]" : "p2_win=$[slot1_win:raw]";
  const win2 =
    side === 1 ? "p2_win=$[slot1_win:raw]" : "p2_win!=$[slot1_win:raw]";
  let query = "";

  if (hasChar && hasName && hasWin) {
    query += oneLine`
      ((p1_char=$[${char}] AND p1_name=$[${name}] AND ${win1})
      OR
      (p2_char=$[${char}] AND p2_name=$[${name}] AND ${win2}))
    `;
  } else if (hasChar && hasWin) {
    query += oneLine`
      ((p1_char=$[${char}] AND ${win1})
      OR
      (p2_char=$[${char}] AND ${win2}))
    `;
  } else if (hasName && hasWin) {
    query += oneLine`
      ((p1_name=$[${name}] AND ${win1})
      OR
      (p2_name=$[${name}] AND ${win2}))
    `;
  } else if (hasChar && hasName) {
    query += oneLine`
      ((p1_char=$[${char}] AND p1_name=$[${name}])
      OR
      (p2_char=$[${char}] AND p2_name=$[${name}]))
    `;
  } else if (hasChar) {
    query += `((p1_char=$[${char}]) OR (p2_char=$[${char}]))`;
  } else if (hasName) {
    query += `((p1_name=$[${name}]) OR (p2_name=$[${name}]))`;
  }
  return query;
}
