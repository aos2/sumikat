import Koa from "koa";
import bodyParser from "koa-bodyparser";
import cors from "@koa/cors";
import { errorHandler, execute } from "graphql-api-koa";
import schema from "./schema.mjs";

const port = 4000;
const server = new Koa()
  .use(errorHandler())
  .use(bodyParser())
  .use(
    cors({
      origin: "*",
      allowMethods: "POST",
      allowHeaders: ["Accept", "Content-Type"],
      maxAge: 600
    })
  )
  .use(execute({ schema }));

server.listen(port, () => {
  console.log(`Listening on port ${port}.`);
});
