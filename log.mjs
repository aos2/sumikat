import { spawn } from "child_process";
import { format } from "util";

export function log(data, ...args) {
  console.log(data, ...args);
}

export function error(data, ...args) {
  const msg = format(data, ...args);
  console.error(msg);
  spawn("logger", [msg]);
}

export const info = error.bind();
export const warn = error.bind();
